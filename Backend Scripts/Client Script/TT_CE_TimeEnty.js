/**
 * @NApiVersion 2.x
 * @NScriptType ClientScript
 * @NModuleScope SameAccount
 */
define(['N/url', 'N/https'],

function(url, https) {
    
    /**
     * Function to be executed after page is initialized.
     *
     * @param {Object} scriptContext
     * @param {Record} scriptContext.currentRecord - Current form record
     * @param {string} scriptContext.mode - The mode in which the record is being accessed (create, copy, or edit)
     *
     * @since 2015.2
     */
    function pageInit(scriptContext) {
        try{

            var serviceItem = scriptContext.currentRecord.getValue({
                fieldId: 'item'
            });

            var serviceItemText = scriptContext.currentRecord.getText({
                fieldId: 'item'
            });                

            if(!IsNullOrEmpty(serviceItem)){



                var response = getServiceData(serviceItem);

                var responseObj = JSON.parse(response.body);
                //log.debug({title: 'responseObj', details: responseObj});
                
                if (responseObj && responseObj.length > 0 ) {
                    var department = responseObj[0].department;
                    var serviceClass = responseObj[0].serviceClass;
                    //log.debug({title: 'department', details: department});

                    if(department != 'NOT_FOUND' && department != 'FAILED'){
                        scriptContext.currentRecord.setValue({
                            fieldId: 'department',
                            value: department
                        });
                    } else {
                        console.log('Department for: ' + serviceItemText + ' ' + department);
                        scriptContext.currentRecord.setValue({
                            fieldId: 'department',
                            value: ''
                        });                            
                    }

                    if(serviceClass != 'NOT_FOUND' && serviceClass != 'FAILED'){
                        scriptContext.currentRecord.setValue({
                            fieldId: 'class',
                            value: serviceClass
                        });
                    } else {
                        console.log('Class for: ' + serviceItemText + ' ' + serviceClass);
                        scriptContext.currentRecord.setValue({
                            fieldId: 'class',
                            value: ''
                        });                            
                    }

                }                    
            } else {
                scriptContext.currentRecord.setValue({
                    fieldId: 'department',
                    value: ''
                }); 
                
                scriptContext.currentRecord.setValue({
                    fieldId: 'class',
                    value: ''
                }); 
            }               

        }catch(e){

            var errorObj = {
                name: e.name, 
                message: e.message, 
                stack: e.stack
           };

           log.error({
               title: 'ERROR_SETTING_DEPARTMENT_AND_CLASS | TT_CE_TimeEntry | PageInit',
               details: errorObj					
           }); 
        }
    }

    /**
     * Function to be executed when field is changed.
     *
     * @param {Object} scriptContext
     * @param {Record} scriptContext.currentRecord - Current form record
     * @param {string} scriptContext.sublistId - Sublist name
     * @param {string} scriptContext.fieldId - Field name
     * @param {number} scriptContext.lineNum - Line number. Will be undefined if not a sublist or matrix field
     * @param {number} scriptContext.columnNum - Line number. Will be undefined if not a matrix field
     *
     * @since 2015.2
     */
    function fieldChanged(scriptContext) {
        if (scriptContext.fieldId == 'item') {

            try{

                if(scriptContext.sublistId && scriptContext.sublistId == 'timeitem'){ //handling weekly time entries

                    var serviceItem = scriptContext.currentRecord.getCurrentSublistValue({
                        sublistId: 'timeitem',
                        fieldId: 'item'
                    });
        
                    var serviceItemText = scriptContext.currentRecord.getCurrentSublistText({
                        sublistId: 'timeitem',
                        fieldId: 'item'
                    });            
                   
                    if(!IsNullOrEmpty(serviceItem)){
    
                        var response = getServiceData(serviceItem);
        
                        var responseObj = JSON.parse(response.body);
                        //log.debug({title: 'responseObj', details: responseObj});
                        
                        if (responseObj && responseObj.length > 0 ) {
                            var department = responseObj[0].department;
                            var serviceClass = responseObj[0].serviceClass;
        
                            if(department != 'NOT_FOUND' && department != 'FAILED'){
        
                                scriptContext.currentRecord.setCurrentSublistValue({
                                    sublistId: 'timeitem',
                                    fieldId: 'department',
                                    value: department,
                                    ignoreFieldChange: false
                                });                        
                            } else {
                                console.log('Department for: ' + serviceItemText + ' ' + department);
                                scriptContext.currentRecord.setCurrentSublistValue({
                                    sublistId: 'timeitem',
                                    fieldId: 'department',
                                    value: '',
                                    ignoreFieldChange: false
                                });                           
                            }

                            if(serviceClass != 'NOT_FOUND' && serviceClass != 'FAILED'){
        
                                scriptContext.currentRecord.setCurrentSublistValue({
                                    sublistId: 'timeitem',
                                    fieldId: 'class',
                                    value: serviceClass,
                                    ignoreFieldChange: false
                                });                        
                            } else {
                                console.log('Class for: ' + serviceItemText + ' ' + serviceClass);
                                scriptContext.currentRecord.setCurrentSublistValue({
                                    sublistId: 'timeitem',
                                    fieldId: 'class',
                                    value: '',
                                    ignoreFieldChange: false
                                });                           
                            }
        
                        }                    
                    } else {
                        scriptContext.currentRecord.setCurrentSublistValue({
                            sublistId: 'timeitem',
                            fieldId: 'department',
                            value: '',
                            ignoreFieldChange: false
                        });
                        
                        scriptContext.currentRecord.setCurrentSublistValue({
                            sublistId: 'timeitem',
                            fieldId: 'class',
                            value: '',
                            ignoreFieldChange: false
                        });
                    }                
    
                } else {

                    var serviceItem = scriptContext.currentRecord.getValue({
                        fieldId: 'item'
                    });
    
                    var serviceItemText = scriptContext.currentRecord.getText({
                        fieldId: 'item'
                    });                
        
                    if(!IsNullOrEmpty(serviceItem)){
                        
                        console.log('serviceItem fieldchanged: ' + serviceItem);
                        var response = getServiceData(serviceItem);

                        var responseObj = JSON.parse(response.body);
                        //log.debug({title: 'responseObj', details: responseObj});
                        
                        if (responseObj && responseObj.length > 0 ) {
                            var department = responseObj[0].department;
                            var serviceClass = responseObj[0].serviceClass;
                            //log.debug({title: 'department', details: department});
    
                            if(department != 'NOT_FOUND' && department != 'FAILED'){
                                scriptContext.currentRecord.setValue({
                                    fieldId: 'department',
                                    value: department
                                });
                            } else {
                                console.log('Department for: ' + serviceItemText + ' ' + department);
                                scriptContext.currentRecord.setValue({
                                    fieldId: 'department',
                                    value: ''
                                });                            
                            }

                            if(serviceClass != 'NOT_FOUND' && serviceClass != 'FAILED'){
                                scriptContext.currentRecord.setValue({
                                    fieldId: 'class',
                                    value: serviceClass
                                });
                            } else {
                                console.log('Class for: ' + serviceItemText + ' ' + serviceClass);
                                scriptContext.currentRecord.setValue({
                                    fieldId: 'class',
                                    value: ''
                                });                            
                            }
    
                        }                    
                    } else {
                        scriptContext.currentRecord.setValue({
                            fieldId: 'department',
                            value: ''
                        }); 
                        
                        scriptContext.currentRecord.setValue({
                            fieldId: 'class',
                            value: ''
                        }); 
                    }                    
                }               

            }catch(e){

                var errorObj = {
                    name: e.name, 
                    message: e.message, 
                    stack: e.stack
               };
    
               log.error({
                   title: 'ERROR_SETTING_DEPARTMENT_AND_CLASS | TT_CE_TimeEntry | Field Changed',
                   details: errorObj					
               }); 
            }
        }
    }

    /**
     * Function to be executed when field is slaved.
     *
     * @param {Object} scriptContext
     * @param {Record} scriptContext.currentRecord - Current form record
     * @param {string} scriptContext.sublistId - Sublist name
     * @param {string} scriptContext.fieldId - Field name
     *
     * @since 2015.2
     */
    function postSourcing(scriptContext) {

    }

    /**
     * Function to be executed after sublist is inserted, removed, or edited.
     *
     * @param {Object} scriptContext
     * @param {Record} scriptContext.currentRecord - Current form record
     * @param {string} scriptContext.sublistId - Sublist name
     *
     * @since 2015.2
     */
    function sublistChanged(scriptContext) {

    }

    /**
     * Function to be executed after line is selected.
     *
     * @param {Object} scriptContext
     * @param {Record} scriptContext.currentRecord - Current form record
     * @param {string} scriptContext.sublistId - Sublist name
     *
     * @since 2015.2
     */
    function lineInit(scriptContext) {

    }

    /**
     * Validation function to be executed when field is changed.
     *
     * @param {Object} scriptContext
     * @param {Record} scriptContext.currentRecord - Current form record
     * @param {string} scriptContext.sublistId - Sublist name
     * @param {string} scriptContext.fieldId - Field name
     * @param {number} scriptContext.lineNum - Line number. Will be undefined if not a sublist or matrix field
     * @param {number} scriptContext.columnNum - Line number. Will be undefined if not a matrix field
     *
     * @returns {boolean} Return true if field is valid
     *
     * @since 2015.2
     */
    function validateField(scriptContext) {

    }

    /**
     * Validation function to be executed when sublist line is committed.
     *
     * @param {Object} scriptContext
     * @param {Record} scriptContext.currentRecord - Current form record
     * @param {string} scriptContext.sublistId - Sublist name
     *
     * @returns {boolean} Return true if sublist line is valid
     *
     * @since 2015.2
     */
    function validateLine(scriptContext) {

    }

    /**
     * Validation function to be executed when sublist line is inserted.
     *
     * @param {Object} scriptContext
     * @param {Record} scriptContext.currentRecord - Current form record
     * @param {string} scriptContext.sublistId - Sublist name
     *
     * @returns {boolean} Return true if sublist line is valid
     *
     * @since 2015.2
     */
    function validateInsert(scriptContext) {

    }

    /**
     * Validation function to be executed when record is deleted.
     *
     * @param {Object} scriptContext
     * @param {Record} scriptContext.currentRecord - Current form record
     * @param {string} scriptContext.sublistId - Sublist name
     *
     * @returns {boolean} Return true if sublist line is valid
     *
     * @since 2015.2
     */
    function validateDelete(scriptContext) {

    }

    /**
     * Validation function to be executed when record is saved.
     *
     * @param {Object} scriptContext
     * @param {Record} scriptContext.currentRecord - Current form record
     * @returns {boolean} Return true if record is valid
     *
     * @since 2015.2
     */
    function saveRecord(scriptContext) {

    }

    function IsNullOrEmpty(_value) {
        if (typeof _value === undefined) {
            return true;
        } else if (util.isString(_value)) {
            if (_value.trim() === "" || _value.length === 0) {
                return true;
            }
        } else if (util.isArray(_value)) {
            if (_value.length === 0) {
                return true;
            } else if (_value.length === 1) {
                //checking is array has a single empty value
                if (_value[0] === '') {
                    return true;
                }
            }
        } else if (util.isObject(_value)) {
            for (var key in _value) {
                if (_value.hasOwnProperty(key)) {
                    return false;
                }
            }
            return true;
        } else if (_value === null) {
            return true;
        } else if (_value == '') {
            return true;
        } else if (_value == null) {
            return true;
        }
        return false;
    } 
    
    /**
     * 
     * @param {number} serviceItem 
     * @returns department and class from the service passed
     */
    function getServiceData(serviceItem){

        var suiteletURL = url.resolveScript({
            scriptId: 'customscript_tt_slet_timeentry',
            deploymentId: 'customdeploy_tt_slet_timeentry',
        });

        var parameters = {
            serviceitem: serviceItem,
        };

        var response = https.post({
            url: suiteletURL,
            body: parameters,
        });

        return response;
    }    

    return {
        pageInit: pageInit,
        fieldChanged: fieldChanged,
        //postSourcing: postSourcing,
        //sublistChanged: sublistChanged,
        //lineInit: lineInit,
        //validateField: validateField, this function has an issue
        //validateLine: validateLine,
        //validateInsert: validateInsert,
        //validateDelete: validateDelete,
        //saveRecord: saveRecord
    };
    
});
