/**
 * @NApiVersion 2.x
 * @NScriptType ClientScript
 * @NModuleScope SameAccount
 * @NAmdConfig /SuiteApps/Tavano Team/2.1/Library/TT_AMD_CONFIG.json
 */

const SCRIPT_PARAMETERS = {
    TIME_LABELS: ['TOTAL UTILIZED', 'TOTAL PRODUCTIVE', 'SUPPORT UTILIZED' , 'SUPPORT PRODUCTIVE', 'DR UTILIZED', 'DR PRODUCTIVE', 'FIX BID UTILIZED', 'FIX BID PRODUCTIVE', 'TOTAL UTILIZED LAST YEAR', 'TOTAL PRODUCTIVE LAST YEAR', 'TOTAL UTILIZED THIS YEAR', 'TOTAL PRODUCTIVE THIS YEAR']
};

define(['N/currentRecord','chart'],

function(currentRecord, chart) {
    //calling custom method
    printGraphic();
    
    /**
     * Function to print graphic
     *
     */
    function printGraphic() {

        try{

            currentRecord = currentRecord.get();

            ///commenting code for quarter graphic 

         /**  var quarterLabels = currentRecord.getValue({ fieldId: 'custentity_ac_quarter_labels' });
            var averagePerQuarterData = currentRecord.getValue({ fieldId: 'custentity_ac_average_per_quarter' });
    
            console.log('quarterLabels: ' + quarterLabels);
            console.log('averagePerQuarterData: ' + averagePerQuarterData);

            if(quarterLabels && averagePerQuarterData){
                quarterLabels = JSON.parse(quarterLabels);
                averagePerQuarterData = JSON.parse(averagePerQuarterData);

                var toolTipAverage = 'Average Revenue';
                var chartTitleAverage = 'Avg Rev Per Month Per Quarter';                
    
                //Colors with opacity :)
                // var barColorsRGBA = ['rgba(251,216,243,0.8)','rgba(188,232,234,0.8)','rgba(255,255,170,0.8)','rgba(230,219,255,0.8)','rgba(194,213,237,0.8)',
                //                      'rgba(251,216,243,0.8)','rgba(188,232,234,0.8)','rgba(255,255,170,0.8)','rgba(230,219,255,0.8)','rgba(194,213,237,0.8)'];
        
                var barColorsRGBA = ['rgba(194,213,237,0.8)','rgba(194,213,237,0.8)','rgba(194,213,237,0.8)','rgba(194,213,237,0.8)','rgba(194,213,237,0.8)',
                    'rgba(194,213,237,0.8)','rgba(194,213,237,0.8)','rgba(194,213,237,0.8)','rgba(194,213,237,0.8)','rgba(194,213,237,0.8)'];
        
                //setting ogh graphic | first subtab
                var dataChartOGH = {
                    idElement: 'bar-chart-average-revenue',
                    quarterLabels: quarterLabels,
                    data: averagePerQuarterData,
                    toolTip: toolTipAverage,
                    titleText: chartTitleAverage,
                    barColorsRGBA: barColorsRGBA
                };
        
                createChart(dataChartOGH); 
            } else{
                document.getElementById('bar-chart-average-revenue').style.display = 'none';
            }

            **/

            //setting amount per month 

            var monthLabels = currentRecord.getValue({ fieldId: 'custentity_ac_month_labels' });
            var amountPerMonthData = currentRecord.getValue({ fieldId: 'custentity_ac_amount_per_month' });
    
            console.log('monthLabels: ' + monthLabels);
            console.log('amountPerMonthData: ' + amountPerMonthData);

            if(monthLabels && amountPerMonthData){
                monthLabels = JSON.parse(monthLabels);
                amountPerMonthData = JSON.parse(amountPerMonthData);

                var toolTipRevenue = 'Revenue per month';
                var chartTitleRevenue = 'Revenue per month';                
    
                //Colors with opacity :)
                // var barColorsRGBA = ['rgba(251,216,243,0.8)','rgba(188,232,234,0.8)','rgba(255,255,170,0.8)','rgba(230,219,255,0.8)','rgba(194,213,237,0.8)',
                //                      'rgba(251,216,243,0.8)','rgba(188,232,234,0.8)','rgba(255,255,170,0.8)','rgba(230,219,255,0.8)','rgba(194,213,237,0.8)'];
        
                var barColorsRGBA = ['rgba(194,213,237,0.8)','rgba(194,213,237,0.8)','rgba(194,213,237,0.8)','rgba(194,213,237,0.8)','rgba(194,213,237,0.8)',
                    'rgba(194,213,237,0.8)','rgba(194,213,237,0.8)','rgba(194,213,237,0.8)','rgba(194,213,237,0.8)','rgba(194,213,237,0.8)'];
        
                //setting ogh graphic | first subtab
                var dataChartOGHMonthly = {
                    idElement: 'bar-chart-revenue-per-month',
                    quarterLabels: monthLabels,
                    data: amountPerMonthData,
                    toolTip: toolTipRevenue,
                    titleText: chartTitleRevenue,
                    barColorsRGBA: barColorsRGBA
                };
        
                createChart(dataChartOGHMonthly); 
            } else{
                document.getElementById('bar-chart-revenue-per-month').style.display = 'none';
            }

            
            //setting time analysis graphic | third subtab
    
            var totalTimeData = currentRecord.getValue({ fieldId: 'custentity_ac_total_time_values' });
            console.log('total time: ' + totalTimeData);

            timeLabels = SCRIPT_PARAMETERS.TIME_LABELS;

            if(totalTimeData){
                totalTimeData = JSON.parse(totalTimeData);
        
                var toolTipTime = 'Total Time';
                var chartTitleTime = 'Time Utilized vs Time Productive';                    
        
                var barColorsRGBATime = ['rgba(194,213,237,0.8)','rgba(251,216,243,0.8)','rgba(194,213,237,0.8)','rgba(251,216,243,0.8)','rgba(194,213,237,0.8)',
                'rgba(251,216,243,0.8)','rgba(194,213,237,0.8)','rgba(251,216,243,0.8)','rgba(194,213,237,0.8)','rgba(251,216,243,0.8)', 'rgba(194,213,237,0.8)','rgba(251,216,243,0.8)'];        
        
                var dataChartTime = {
                    idElement: 'bar-chart-time-analysis',
                    quarterLabels: timeLabels,
                    data: totalTimeData,
                    toolTip: toolTipTime,
                    titleText: chartTitleTime,
                    barColorsRGBA: barColorsRGBATime
                };
        
                createChart(dataChartTime); 
            } else {
                document.getElementById('bar-chart-time-analysis').style.display = 'none'; 
            }           

        } catch(e) {
            var errorObj = {
                name: e.name, 
                message: e.message, 
                stack: e.stack
           };

           console.error('ERROR_TT_CE_AccountAnalysis: ' + errorObj);
        }        

    }

    function createChart(dataChart){

        // Define a plugin to provide data labels
        chart.plugins.register({
            afterDatasetsDraw: function(chart) {
                var ctx = chart.ctx;

                chart.data.datasets.forEach(function(dataset, i) {
                    var meta = chart.getDatasetMeta(i);
                    if (!meta.hidden) {
                        meta.data.forEach(function(element, index) {
                            // Draw the text in black, with the specified font
                            ctx.fillStyle = 'rgb(0, 0, 0)';

                            var fontSize = 14;
                            var fontStyle = 'normal';
                            var fontFamily = 'Helvetica Neue';
                            ctx.font = Chart.helpers.fontString(fontSize, fontStyle, fontFamily);

                            // Just naively convert to string for now
                            var dataString = dataset.data[index].toString();

                            // Make sure alignment settings are correct
                            ctx.textAlign = 'center';
                            ctx.textBaseline = 'middle';

                            var padding = 5;
                            var position = element.tooltipPosition();
                            ctx.fillText(dataString, position.x, position.y - (fontSize / 2) - padding);
                        });
                    }
                });
            }
        });

        new chart(document.getElementById(dataChart.idElement), {
            type: 'bar',
            data: {
                labels: dataChart.quarterLabels,
                datasets: [
                    {
                        label: dataChart.toolTip,
                        backgroundColor: dataChart.barColorsRGBA,
                        data: dataChart.data
                    }
                ]
            },
            options: {
                legend: { display: false },
                title: {
                    display: true,
                    text: dataChart.titleText
                },
                scales: {
                    yAxes: [{
                        ticks: {
                            beginAtZero:true
                        }
                    }]
                }
            }
        });
    } 
    
    /**
     * Function to be executed when field is changed.
     *
     * @param {Object} scriptContext
     * @param {Record} scriptContext.currentRecord - Current form record
     * @param {string} scriptContext.sublistId - Sublist name
     * @param {string} scriptContext.fieldId - Field name
     * @param {number} scriptContext.lineNum - Line number. Will be undefined if not a sublist or matrix field
     * @param {number} scriptContext.columnNum - Line number. Will be undefined if not a matrix field
     *
     * @since 2015.2
     */
    function fieldChanged(scriptContext) {

    }    

    return {
        fieldChanged: fieldChanged
    };
    
});
