/**
 * @NApiVersion 2.x
 * @NScriptType ClientScript
 * @NModuleScope SameAccount
 */
define(['N/url', 'N/https'],

function(url, https) {
    
    /**
     * Function to be executed after page is initialized.
     *
     * @param {Object} scriptContext
     * @param {Record} scriptContext.currentRecord - Current form record
     * @param {string} scriptContext.mode - The mode in which the record is being accessed (create, copy, or edit)
     *
     * @since 2015.2
     */
    function pageInit(scriptContext) {
        try{

            console.log('page init test');              

        }catch(e){

            var errorObj = {
                name: e.name, 
                message: e.message, 
                stack: e.stack
           };

           log.error({
               title: 'ERROR_SETTING_DEPARTMENT | TT_CE_TimeEntry | PageInit',
               details: errorObj					
           }); 
        }
    }

    /**
     * Function to be executed when field is changed.
     *
     * @param {Object} scriptContext
     * @param {Record} scriptContext.currentRecord - Current form record
     * @param {string} scriptContext.sublistId - Sublist name
     * @param {string} scriptContext.fieldId - Field name
     * @param {number} scriptContext.lineNum - Line number. Will be undefined if not a sublist or matrix field
     * @param {number} scriptContext.columnNum - Line number. Will be undefined if not a matrix field
     *
     * @since 2015.2
     */
    function fieldChanged(scriptContext) {


        if(scriptContext.fieldId == 'item'){

            if(scriptContext.sublistId && scriptContext.sublistId == 'timeitem'){

                var serviceItem = scriptContext.currentRecord.getCurrentSublistValue({
                    sublistId: 'timeitem',
                    fieldId: 'item'
                });
    
                var serviceItemText = scriptContext.currentRecord.getCurrentSublistText({
                    sublistId: 'timeitem',
                    fieldId: 'item'
                });            
               
                if(!IsNullOrEmpty(serviceItem)){

                    var response = getDepartment(serviceItem);
    
                    var responseObj = JSON.parse(response.body);
                    //log.debug({title: 'responseObj', details: responseObj});
                    
                    if (responseObj && responseObj.length > 0 ) {
                        var department = responseObj[0].department;
    
                        if(department != 'NOT_FOUND' && department != 'FAILED'){
    
                            scriptContext.currentRecord.setCurrentSublistValue({
                                sublistId: 'timeitem',
                                fieldId: 'department',
                                value: department,
                                ignoreFieldChange: false
                            });                        
                        } else {
                            alert('Department for: ' + serviceItemText + ' ' + department);
                            scriptContext.currentRecord.setCurrentSublistValue({
                                sublistId: 'timeitem',
                                fieldId: 'department',
                                value: '',
                                ignoreFieldChange: false
                            });                           
                        }
    
                    }                    
                } else {
                    scriptContext.currentRecord.setCurrentSublistValue({
                        sublistId: 'timeitem',
                        fieldId: 'department',
                        value: '',
                        ignoreFieldChange: false
                    });                   
                }                

            }
            
        }

        
    }

    /**
     * Function to be executed when field is slaved.
     *
     * @param {Object} scriptContext
     * @param {Record} scriptContext.currentRecord - Current form record
     * @param {string} scriptContext.sublistId - Sublist name
     * @param {string} scriptContext.fieldId - Field name
     *
     * @since 2015.2
     */
    function postSourcing(scriptContext) {

    }

    /**
     * Function to be executed after sublist is inserted, removed, or edited.
     *
     * @param {Object} scriptContext
     * @param {Record} scriptContext.currentRecord - Current form record
     * @param {string} scriptContext.sublistId - Sublist name
     *
     * @since 2015.2
     */
    function sublistChanged(scriptContext) {

        if(scriptContext.sublistId == 'timeitem'){

            var serviceItem = scriptContext.currentRecord.getCurrentSublistValue({
                sublistId: 'timeitem',
                fieldId: 'item'
            });   
            console.log('serviceItemLine: ' + serviceItem) ; 

            var serviceItemText = scriptContext.currentRecord.getCurrentSublistText({
                sublistId: 'timeitem',
                fieldId: 'item'
            });   
            console.log('serviceItemLineText: ' + serviceItemText) ;             
           
            
            if(!IsNullOrEmpty(serviceItem)){

                var suiteletURL = url.resolveScript({
                    scriptId: 'customscript_tt_slet_timeentry',
                    deploymentId: 'customdeploy_tt_slet_timeentry',
                });

                var parameters = {
                    serviceitem: serviceItem,
                };

                var response = https.post({
                    url: suiteletURL,
                    body: parameters,
                });

                var responseObj = JSON.parse(response.body);
                //log.debug({title: 'responseObj', details: responseObj});
                
                if (responseObj && responseObj.length > 0 ) {
                    var department = responseObj[0].department;
                    console.log('department: ' + department);

                    if(department != 'NOT_FOUND' && department != 'FAILED'){

                        scriptContext.currentRecord.setCurrentSublistValue({
                            sublistId: 'timeitem',
                            fieldId: 'department',
                            value: department,
                            ignoreFieldChange: false
                        });                        
                    } else {
                        alert('Department for: ' + serviceItemText + ' ' + department);
                        scriptContext.currentRecord.setCurrentSublistValue({
                            sublistId: 'timeitem',
                            fieldId: 'department',
                            value: '',
                            ignoreFieldChange: false
                        });                           
                    }

                }                    
            } else {
                scriptContext.currentRecord.setCurrentSublistValue({
                    sublistId: 'timeitem',
                    fieldId: 'department',
                    value: '',
                    ignoreFieldChange: false
                });                   
            }            
        }
        

        console.log('scriptContext.sublistId: ' + scriptContext.sublistId) ;

        console.log('scriptContext.operation: ' + scriptContext.operation) ;
    }

    /**
     * Function to be executed after line is selected.
     *
     * @param {Object} scriptContext
     * @param {Record} scriptContext.currentRecord - Current form record
     * @param {string} scriptContext.sublistId - Sublist name
     *
     * @since 2015.2
     */
    function lineInit(scriptContext) {
        console.log('line init');
    }

    /**
     * Validation function to be executed when field is changed.
     *
     * @param {Object} scriptContext
     * @param {Record} scriptContext.currentRecord - Current form record
     * @param {string} scriptContext.sublistId - Sublist name
     * @param {string} scriptContext.fieldId - Field name
     * @param {number} scriptContext.lineNum - Line number. Will be undefined if not a sublist or matrix field
     * @param {number} scriptContext.columnNum - Line number. Will be undefined if not a matrix field
     *
     * @returns {boolean} Return true if field is valid
     *
     * @since 2015.2
     */
    function validateField(scriptContext) {

    }

    /**
     * Validation function to be executed when sublist line is committed.
     *
     * @param {Object} scriptContext
     * @param {Record} scriptContext.currentRecord - Current form record
     * @param {string} scriptContext.sublistId - Sublist name
     *
     * @returns {boolean} Return true if sublist line is valid
     *
     * @since 2015.2
     */
    function validateLine(scriptContext) {

    }

    /**
     * Validation function to be executed when sublist line is inserted.
     *
     * @param {Object} scriptContext
     * @param {Record} scriptContext.currentRecord - Current form record
     * @param {string} scriptContext.sublistId - Sublist name
     *
     * @returns {boolean} Return true if sublist line is valid
     *
     * @since 2015.2
     */
    function validateInsert(scriptContext) {

    }

    /**
     * Validation function to be executed when record is deleted.
     *
     * @param {Object} scriptContext
     * @param {Record} scriptContext.currentRecord - Current form record
     * @param {string} scriptContext.sublistId - Sublist name
     *
     * @returns {boolean} Return true if sublist line is valid
     *
     * @since 2015.2
     */
    function validateDelete(scriptContext) {

    }

    /**
     * Validation function to be executed when record is saved.
     *
     * @param {Object} scriptContext
     * @param {Record} scriptContext.currentRecord - Current form record
     * @returns {boolean} Return true if record is valid
     *
     * @since 2015.2
     */
    function saveRecord(scriptContext) {

    }

    function IsNullOrEmpty(_value) {
        if (typeof _value === undefined) {
            return true;
        } else if (util.isString(_value)) {
            if (_value.trim() === "" || _value.length === 0) {
                return true;
            }
        } else if (util.isArray(_value)) {
            if (_value.length === 0) {
                return true;
            } else if (_value.length === 1) {
                //checking is array has a single empty value
                if (_value[0] === '') {
                    return true;
                }
            }
        } else if (util.isObject(_value)) {
            for (var key in _value) {
                if (_value.hasOwnProperty(key)) {
                    return false;
                }
            }
            return true;
        } else if (_value === null) {
            return true;
        } else if (_value == '') {
            return true;
        } else if (_value == null) {
            return true;
        }
        return false;
    }
    
    function getDepartment(serviceItem){

        var suiteletURL = url.resolveScript({
            scriptId: 'customscript_tt_slet_timeentry',
            deploymentId: 'customdeploy_tt_slet_timeentry',
        });

        var parameters = {
            serviceitem: serviceItem,
        };

        var response = https.post({
            url: suiteletURL,
            body: parameters,
        });

        return response;
    }   

    return {
        pageInit: pageInit,
        fieldChanged: fieldChanged,
        //postSourcing: postSourcing,
        //sublistChanged: sublistChanged,
        //lineInit: lineInit,
        //validateField: validateField, this function has an issue
        //validateLine: validateLine,
        //validateInsert: validateInsert,
        //validateDelete: validateDelete,
        //saveRecord: saveRecord
    };
    
});
