/**
 * @NApiVersion 2.1
 * @NScriptType Suitelet
 * @NModuleScope SameAccount
 */
define(['N/search'],

function(search) {
   
    /**
     * Definition of the Suitelet script trigger point.
     *
     * @param {Object} context
     * @param {ServerRequest} context.request - Encapsulation of the incoming request
     * @param {ServerResponse} context.response - Encapsulation of the Suitelet response
     * @Since 2015.2
     */
    function onRequest(context) {
        const serviceItemParam = context.request.parameters.serviceitem;
        const dataArray = [];

        if(serviceItemParam) {
            // return department

            try {

                const lookUpServiceItem = search.lookupFields({
                    type: search.Type.SERVICE_ITEM,
                    id: serviceItemParam,
                    columns: ['department', 'class']
                }); 


                log.debug({title: 'lookUpServiceItem', details: lookUpServiceItem});
                let department = 'NOT_FOUND';
                let serviceClass = 'NOT_FOUND';
                

                if (lookUpServiceItem && lookUpServiceItem.department && lookUpServiceItem.department.length > 0) {
                    department = lookUpServiceItem.department[0].value;
                    serviceClass = lookUpServiceItem.class[0].value;
                }    
                
                dataArray.push({
                    department: department,
                    serviceClass: serviceClass
                });     

            } catch (e) {
                log.error({title: 'SERICE_ITEM_LOOKUP_ERROR', details: JSON.stringify(e)});
                dataArray.push({
                    department: 'FAILED',
                    serviceClass: 'FAILED'
                });
            }

        } 

        context.response.setHeader({
            name: 'Content-Type',
            value: 'application/json'
        });

        context.response.write(JSON.stringify(dataArray));
    }

    return {
        onRequest: onRequest
    };
    
});