/**
 * @NApiVersion 2.1
 * @NScriptType UserEventScript
 * @NModuleScope SameAccount
 */

const SCRIPT_PARAMETERS = {
    CLIENT_SCRIPT_FILE_ID: null,
    HTML_REVENUE_TEMPLATE_FILE_ID: null,
    HTML_MONTHLY_REVENUE_TEMPLATE_FILE_ID: null,
    HTML_TIME_TEMPLATE_FILE_ID: null
};

define(['N/ui/serverWidget', 'N/file', 'N/runtime', 'N/error'],

function(serverWidget, file, runtime, error) {
   
    /**
     * Function definition to be triggered before record is loaded.
     *
     * @param {Object} scriptContext
     * @param {Record} scriptContext.newRecord - New record
     * @param {string} scriptContext.type - Trigger type
     * @param {Form} scriptContext.form - Current form
     * @Since 2015.2
     */
    function beforeLoad(scriptContext) {

        try {

            getParameters();

            const form = scriptContext.form;
            //setting client script
            form.clientScriptFileId = SCRIPT_PARAMETERS.CLIENT_SCRIPT_FILE_ID;
    
            //Note: module file cannot be used on client script, we are setting the HTML content in a before load event
            //printing the ogh graphic
            //setRevenueTemplateContent(form); 
            //printing the monthly ogh graphic
            setMonthlyRevenueTemplateContent(form);           
            //printing the time analysis graphic
            setTimeTemplateContent(form);    
             

        } catch(e) {

           const errorObj = {
                name: e.name, 
                message: e.message, 
                stack: e.stack
           };

           log.error({
               title: 'ERROR_ACCOUNT_ANALYSIS_SCRIPT',
               details: errorObj					
           }); 
        }       
                
    }

    /**
     * Parameters
     */
    function getParameters() {
        const scriptObj = runtime.getCurrentScript();
        
        SCRIPT_PARAMETERS.CLIENT_SCRIPT_FILE_ID = scriptObj.getParameter({ name: 'custscript_acc_client_script_file' });
        SCRIPT_PARAMETERS.HTML_REVENUE_TEMPLATE_FILE_ID = scriptObj.getParameter({ name: 'custscript_acc_revenue_html_tpl_file' });
        SCRIPT_PARAMETERS.HTML_MONTHLY_REVENUE_TEMPLATE_FILE_ID = scriptObj.getParameter({ name: 'custscript_acc_mth_revenue_html_tpl_file' });	
        SCRIPT_PARAMETERS.HTML_TIME_TEMPLATE_FILE_ID = scriptObj.getParameter({ name: 'custscript_acc_time_html_tpl_file' });

        log.debug({ title: 'SCRIPT_PARAMETERS.HTML_MONTHLY_REVENUE_TEMPLATE_FILE_ID', details: SCRIPT_PARAMETERS.HTML_MONTHLY_REVENUE_TEMPLATE_FILE_ID });
              
        if (isNullOrEmpty(SCRIPT_PARAMETERS.CLIENT_SCRIPT_FILE_ID)) {
            throw error.create({ name: 'EMPTY_PARAMETER', message: 'Error retrieving script parameter CLIENT SCRIPT FILE' });
        }
        if (isNullOrEmpty(SCRIPT_PARAMETERS.HTML_REVENUE_TEMPLATE_FILE_ID)) {
            throw error.create({ name: 'EMPTY_PARAMETER', message: 'Error retrieving script parameter HTML REVENUE TEMPLATE FILE' });
        }
        if (isNullOrEmpty(SCRIPT_PARAMETERS.HTML_MONTHLY_REVENUE_TEMPLATE_FILE_ID)) {
            throw error.create({ name: 'EMPTY_PARAMETER', message: 'Error retrieving script parameter HTML MONTHLY REVENUE TEMPLATE FILE' });
        }
        if (isNullOrEmpty(SCRIPT_PARAMETERS.HTML_TIME_TEMPLATE_FILE_ID)) {
            throw error.create({ name: 'EMPTY_PARAMETER', message: 'Error retrieving script parameter HTML TIME TEMPLATE FILE' });
        }                        
    }   
    
	function isNullOrEmpty(value) {
		if ((value === '') || (value === null) || (typeof value === 'undefined')) {
			return true;
		} else if (value instanceof String) {
			if (value.trim() === '') {
				return true;
			}
		} else if (Array.isArray(value)) {
			if (value.length === 0) {
				return true;
			}
		} else if (typeof value == 'object') {
			for (let key in value) {
				if (value.hasOwnProperty(key)) {
					return false;
				}
			}
			return true;
		} else {
			return false;
		}
    }

    function setRevenueTemplateContent(form){

        const htmlTemplate = form.addField({
            id: 'custpage_revenue_htmltemplate',
            type: serverWidget.FieldType.INLINEHTML,
            label: 'HTML Template',
            container: 'custom27'
        });
        const htmlContent = file.load({ id: SCRIPT_PARAMETERS.HTML_REVENUE_TEMPLATE_FILE_ID}).getContents(); //change this for a parameter 

        htmlTemplate.defaultValue = htmlContent;

        htmlTemplate.updateLayoutType({
            layoutType: serverWidget.FieldLayoutType.OUTSIDEBELOW
        });         

    }

    function setMonthlyRevenueTemplateContent(form){

        const htmlTemplate = form.addField({
            id: 'custpage_monthly_revenue_htmltemplate',
            type: serverWidget.FieldType.INLINEHTML,
            label: 'HTML Template',
            container: 'custom27'
        });

        const htmlContent = file.load({ id: SCRIPT_PARAMETERS.HTML_MONTHLY_REVENUE_TEMPLATE_FILE_ID}).getContents(); //change this for a parameter 

        htmlTemplate.defaultValue = htmlContent;

        htmlTemplate.updateLayoutType({
            layoutType: serverWidget.FieldLayoutType.OUTSIDEBELOW
        });  

    }

    function setTimeTemplateContent(form){

        const htmlTemplate = form.addField({
            id: 'custpage_time_htmltemplate',
            type: serverWidget.FieldType.INLINEHTML,
            label: 'HTML Time Template',
            container: 'custom26'
        });
        const htmlContent = file.load({ id: SCRIPT_PARAMETERS.HTML_TIME_TEMPLATE_FILE_ID}).getContents(); //change this for a parameter 

        htmlTemplate.defaultValue = htmlContent;

        htmlTemplate.updateLayoutType({
            layoutType: serverWidget.FieldLayoutType.OUTSIDEBELOW
        });         

    }
    

    /**
     * Function definition to be triggered before record is loaded.
     *
     * @param {Object} scriptContext
     * @param {Record} scriptContext.newRecord - New record
     * @param {Record} scriptContext.oldRecord - Old record
     * @param {string} scriptContext.type - Trigger type
     * @Since 2015.2
     */
    function beforeSubmit(scriptContext) {

    }

    /**
     * Function definition to be triggered before record is loaded.
     *
     * @param {Object} scriptContext
     * @param {Record} scriptContext.newRecord - New record
     * @param {Record} scriptContext.oldRecord - Old record
     * @param {string} scriptContext.type - Trigger type
     * @Since 2015.2
     */
    function afterSubmit(scriptContext) {
        
    }

    return {
        beforeLoad: beforeLoad,
        beforeSubmit: beforeSubmit,
        afterSubmit: afterSubmit
    };
    
});
