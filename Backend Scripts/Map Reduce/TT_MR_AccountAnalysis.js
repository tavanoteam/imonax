/**
 * @NApiVersion 2.1
 * @NScriptType MapReduceScript
 * @NModuleScope SameAccount
 */

const SCRIPT_PARAMETERS = {
    DO_NOT_APPLY: {
        VALUE: -1,
        LABEL: 'N/A'
    },
    ELITE_STATUS:{
        TOP_HALF_OF_FAME: 1,
        TOP_ALL_START: 2,
        TOP_ALL_START_POTENTIAL: 3,
        TOP_MVP_OF_THE_SEASON: 4,
        MVP_POTENTIAL: 5,
        ROOKIE_OF_THE_YEAR_TEAM: 6,
        NO_STATUS: 7,
    }
};

define(['N/search', 'N/record'],

function(search, record) {
   
    /**
     * Marks the beginning of the Map/Reduce process and generates input data.
     *
     * @typedef {Object} ObjectRef
     * @property {number} id - Internal ID of the record instance
     * @property {string} type - Record type id
     *
     * @return {Array|Object|Search|RecordRef} inputSummary
     * @since 2015.1
     */
    function getInputData() {

        try{

            var customerSearchObj = search.create({
                type: search.Type.CUSTOMER,
                filters:
                [
                   ['isinactive', search.Operator.IS,'F'],
                   'AND',
                   ['status', search.Operator.ANYOF,'13','16','15'] //getting only customers | not prospects, not leads 
                ],
                columns:[
                    search.createColumn({
                        name: 'internalid',
                        sort: search.Sort.ASC,
                        label: 'Internal ID'
                     })                
                ]
             });
             var searchResultCount = customerSearchObj.runPaged().count;
             log.audit('customerSearchObj result count',searchResultCount);


            return customerSearchObj;
        }catch(e){
            
            const errorObj = {
                name: e.name, 
                message: e.message, 
                stack: e.stack
           };

           log.error({
               title: 'ERROR_ACCOUNT_ANALYSIS_GETINPUT_STAGE',
               details: errorObj					
           });
        }

    }

    /**
     * Executes when the map entry point is triggered and applies to each key/value pair.
     *
     * @param {MapSummary} context - Data collection containing the key/value pairs to process through the map stage
     * @since 2015.1
     */
    function map(context) {
        try{
            const savedSearchResult = JSON.parse(context.value);
            const customerid = savedSearchResult.id;
            log.audit({ title: 'savedSearchResult', details: savedSearchResult });

            //first tab
            let averagePerQuarter = _getAverageAmountPerQuarter(customerid); 
            //log.audit( { title: 'averagePerQuarter before', details: averagePerQuarter });

            let amountPerMonth = _getAmountPerMonth(customerid);

            //log.audit( { title: 'amountPerMonth', details: amountPerMonth });

            for(var i=0; i < averagePerQuarter.length; i++){
                let averageAux = averagePerQuarter[i];
                if(!_isNullOrEmpty(averageAux)){ // to avoid NaN values
                    averageAux = parseFloat(averageAux);
                    averagePerQuarter[i] = averageAux.toFixed(2);
                    averagePerQuarter[i] = averagePerQuarter[i].toString();
                }
            }
            //log.audit( { title: 'averagePerQuarter after', details: averagePerQuarter });      
            
            for(var i=0; i < amountPerMonth.length; i++){
                let amountAux = amountPerMonth[i];
                if(!_isNullOrEmpty(amountAux)){ // to avoid NaN values
                    amountAux = parseFloat(amountAux);
                    amountPerMonth[i] = amountAux.toFixed(2);
                    amountPerMonth[i] = amountPerMonth[i].toString();
                }
            }
            //log.audit( { title: 'averagePerQuarter after', details: averagePerQuarter });  

            //second subtab
            const totalAmount = _getTotalInvoiceAmount(customerid);
            const totalAmountLast3Y = _getTotalInvoiceAmount(customerid, lastYears = 3);
    
            //log.debug( { title: 'totalAmount', details: totalAmount });
            //log.debug( { title: 'typeof totalAmount', details: typeof totalAmount });

            const firstInvoiceDate = _getTransactionDate(customerid, 'FIRST');
            log.debug( { title: 'firstInvoiceDate', details: firstInvoiceDate });
            
            const firstInvoiceDateLast3Y = _getTransactionDate(customerid, 'FIRST', lastYears = 3);
            log.debug( { title: 'firstInvoiceDateLast3Y', details: firstInvoiceDateLast3Y });            
    
            const lastInvoiceDate = _getTransactionDate(customerid, 'LAST');
            log.debug( { title: 'lastInvoiceDate', details: lastInvoiceDate });
            
            const lastInvoiceDateLast3Y = _getTransactionDate(customerid, 'LAST', lastYears = 3);
            log.debug( { title: 'lastInvoiceDateLast3Y', details: lastInvoiceDateLast3Y });     

            //adding two extra fields aksed by ralo
            //Avg Rev per Month Last Year
            const firstInvoiceDateLastYear = _getTransactionDate(customerid, 'FIRST', lastYears = 2); 
            let averageRevenuePerMonthLastYear = 0.00;

            if(!_isNullOrEmpty(firstInvoiceDateLastYear)){
                //log.error( { title: 'firstInvoiceDateLastYear', details: firstInvoiceDateLastYear }); 

                const lastInvoiceDateLastYear = _getTransactionDate(customerid, 'LAST', lastYears = 2);
                //log.error( { title: 'customerid', details: customerid });
                //log.error( { title: 'lastInvoiceDateLastYear', details: lastInvoiceDateLastYear }); 
                
                //let monthsBetweenFirstLastInvoiceLastYear = _monthsDiff(firstInvoiceDateLastYear, lastInvoiceDateLastYear);
                //log.error( { title: 'monthsBetweenFirstLastInvoiceLastYear', details: monthsBetweenFirstLastInvoiceLastYear });
                //we add one month to get accurate results
                //example first invoice this year = 1/6/2022	and last invoice this year = 4/29/2022	will return 3
                //but we need to divide by 4 (January, February, March and April)
                //monthsBetweenFirstLastInvoiceLastYear = monthsBetweenFirstLastInvoiceLastYear + 1;
                
                const totalAmountLastYear = _getTotalInvoiceAmount(customerid, lastYears = 2);
                //log.error( { title: 'totalAmountLastYear', details: totalAmountLastYear });

                //for old clients who first invoice ever was berore last year, the lastYearMonths = 12
                let lastYearMonths = 12;

                if(firstInvoiceDate){
                    var yearFirstInvoice = (new Date(firstInvoiceDate)).getFullYear();
                    var lastYear = (new Date()).getFullYear() - 1;
                    var difference2 = lastYear - yearFirstInvoice;

                    log.error( { title: 'yearFirstInvoice', details: yearFirstInvoice });
                    log.error( { title: 'lastYear', details: lastYear });
                    log.error( { title: 'difference2', details: difference2 });

                    if(difference2 === 0){
                        //it starts in 0 and ends in 11
                        let monthFirstInvoiceLastYear = (new Date(firstInvoiceDateLastYear)).getMonth();
                        log.error( { title: 'monthFirstInvoiceLastYear', details: monthFirstInvoiceLastYear });
                        lastYearMonths = 12 - monthFirstInvoiceLastYear;

                        log.error( { title: 'monthFirstInvoiceLastYear', details: monthFirstInvoiceLastYear });
                        log.error( { title: 'lastYearMonths', details: lastYearMonths });
                    }

                }
                
                averageRevenuePerMonthLastYear = totalAmountLastYear / lastYearMonths; //change requested by Ralo

                //log.error( { title: 'averageRevenuePerMonthLastYear', details: averageRevenuePerMonthLastYear });
            }            

            //Avg Rev. per Month This Year   
            const firstInvoiceDateThisYear = _getTransactionDate(customerid, 'FIRST', lastYears = 1); 
            //log.error( { title: 'firstInvoiceDateThisYear', details: firstInvoiceDateThisYear }); 
            let averageRevenuePerMonthThisYear = 0.00;
            let clvProgres = 0.00;

            if(!_isNullOrEmpty(firstInvoiceDateThisYear)){
                //log.error( { title: 'firstInvoiceDateThisYear', details: firstInvoiceDateThisYear }); 

                //const lastInvoiceDateThisYear = _getTransactionDate(customerid, 'LAST', lastYears = 1);
                //log.error( { title: 'customerid', details: customerid });
                //log.error( { title: 'lastInvoiceDateThisYear', details: lastInvoiceDateThisYear }); 
                
                //let monthsBetweenFirstLastInvoiceThisYear = _monthsDiff(firstInvoiceDateThisYear, lastInvoiceDateThisYear);
                //log.error( { title: 'monthsBetweenFirstLastInvoiceThisYear', details: monthsBetweenFirstLastInvoiceThisYear });
                //we add one month to get accurate results
                //example first invoice this year = 1/6/2022	and last invoice this year = 4/29/2022	will return 3
                //but we need to divide by 4 (January, February, March and April)
                //monthsBetweenFirstLastInvoiceThisYear = monthsBetweenFirstLastInvoiceThisYear + 1;


                let now = new Date();
                let monthsBetweenFirstInvoiceThisYearUntilNow = _monthsDiff(firstInvoiceDateThisYear, now);
                //log.error( { title: 'monthsBetweenFirstInvoiceThisYearUntilNow', details: monthsBetweenFirstInvoiceThisYearUntilNow });
                
                monthsBetweenFirstInvoiceThisYearUntilNow = monthsBetweenFirstInvoiceThisYearUntilNow + 1;

                const totalAmountThisYear = _getTotalInvoiceAmount(customerid, lastYears = 1);
                //log.error( { title: 'totalAmountThisYear', details: totalAmountThisYear });
                
                averageRevenuePerMonthThisYear = totalAmountThisYear / monthsBetweenFirstInvoiceThisYearUntilNow;

                //log.error( { title: 'averageRevenuePerMonthThisYear', details: averageRevenuePerMonthThisYear });


                //for old clients who first invoice ever was berore this year, the clvMonths = 12
                let clvMonths = 12;

                if(firstInvoiceDate){
                    var yearFirstInvoice = (new Date(firstInvoiceDate)).getFullYear();
                    var currentYear = (new Date()).getFullYear();
                    var difference = currentYear - yearFirstInvoice;



                    if(difference === 0){
                        //it starts in 0 and ends in 11
                        let monthFirstInvoiceThisYear = (new Date(firstInvoiceDateThisYear)).getMonth();
                        //log.error( { title: 'monthFirstInvoiceThisYear', details: monthFirstInvoiceThisYear });
                        clvMonths = 12 - monthFirstInvoiceThisYear;
                    }

                }

                //let numberOfMonths = (new Date(firstInvoiceDateThisYear)).getMonth()

                clvProgres = totalAmountThisYear/ clvMonths;
            }
            
            let monthsInactive;
            let monthsActive;
            let monthsActiveLast3Y;
            let revenuePerActiveMonth;
            let revenuePerActiveMonthLast3Y;
            let eliteStatus;
    
            if(lastInvoiceDate != SCRIPT_PARAMETERS.DO_NOT_APPLY.VALUE){
                monthsInactive = _monthsDiff(lastInvoiceDate, new Date());
                //log.debug( { title: 'monthsInactive', details: monthsInactive }); 
            }else{
                monthsInactive = SCRIPT_PARAMETERS.DO_NOT_APPLY.VALUE;
            }
    
            if(firstInvoiceDate != SCRIPT_PARAMETERS.DO_NOT_APPLY.VALUE && lastInvoiceDate != SCRIPT_PARAMETERS.DO_NOT_APPLY.VALUE){
                monthsActive = _monthsDiff(firstInvoiceDate, lastInvoiceDate);
                //if first invoice date = February 10 and last invoice date is = Junio 1
                // monthsActive = 6 -2 = 4 
                // Ralo wants to include the first month (February) to appear in the results, we need to add +1 
                monthsActive = monthsActive + 1;
                log.debug( { title: 'monthsActive', details: monthsActive }); 
            }else{
                monthsActive = SCRIPT_PARAMETERS.DO_NOT_APPLY.VALUE;
            }

            if(firstInvoiceDateLast3Y != SCRIPT_PARAMETERS.DO_NOT_APPLY.VALUE && lastInvoiceDateLast3Y != SCRIPT_PARAMETERS.DO_NOT_APPLY.VALUE){
                //if first invoice date = February 10 and last invoice date is = Junio 1
                // monthsActive = 6 -2 = 4 
                // Ralo wants to include the first month (February) to appear in the results, we need to add +1 
                monthsActiveLast3Y = _monthsDiff(firstInvoiceDateLast3Y, lastInvoiceDateLast3Y);
                monthsActiveLast3Y = monthsActiveLast3Y + 1;
                log.debug( { title: 'monthsActiveLast3Y', details: monthsActiveLast3Y }); 
            }else{
                monthsActiveLast3Y = SCRIPT_PARAMETERS.DO_NOT_APPLY.VALUE;
            }            
    
            if(monthsActive != SCRIPT_PARAMETERS.DO_NOT_APPLY.VALUE && monthsActive > 0){
                revenuePerActiveMonth = totalAmount/monthsActive;
                //log.audit( { title: 'totalAmount', details: totalAmount });
                //log.audit( { title: 'monthsActive', details: monthsActive });
                //log.audit( { title: 'revenuePerActiveMonth', details: revenuePerActiveMonth }); 
            }else{
                revenuePerActiveMonth = 0.00;
                if(monthsActive == 0){
                    revenuePerActiveMonth = totalAmount; //it means a unique invoice | first and last invoice is on the same date
                    //ralo asked to display 1 if there was a single invoice 
                    monthsActive = 1;
                }
            }
            
            if(monthsActiveLast3Y != SCRIPT_PARAMETERS.DO_NOT_APPLY.VALUE && monthsActiveLast3Y > 0){
                revenuePerActiveMonthLast3Y = totalAmountLast3Y/monthsActiveLast3Y;
                //log.debug( { title: 'revenuePerActiveMonthLast3Y', details: revenuePerActiveMonthLast3Y }); 
            }else{
                revenuePerActiveMonthLast3Y = 0.00;
                if(monthsActiveLast3Y == 0){
                    revenuePerActiveMonthLast3Y = totalAmountLast3Y; //it means a unique invoice | first and last invoice is on the same date
                    //ralo asked to display 1 if there was a single invoice
                    monthsActiveLast3Y = 1;
                }
            }            
            
            if(monthsActiveLast3Y != SCRIPT_PARAMETERS.DO_NOT_APPLY.VALUE){
                if(monthsActiveLast3Y > 24){
                    if(revenuePerActiveMonthLast3Y > 15000){
                        //log.debug( { title: 'elite status', details: 'Top - hall of fame' });
                        eliteStatus = SCRIPT_PARAMETERS.ELITE_STATUS.TOP_HALF_OF_FAME; 
                    } else if(revenuePerActiveMonthLast3Y > 5000) {
                        //log.debug( { title: 'elite status', details: 'Top - all start' }); 
                        eliteStatus = SCRIPT_PARAMETERS.ELITE_STATUS.TOP_ALL_START;
                    } else if(revenuePerActiveMonthLast3Y > 3000){
                       // log.debug( { title: 'elite status', details: 'Top - all start potential' }); 
                        eliteStatus = SCRIPT_PARAMETERS.ELITE_STATUS.TOP_ALL_START_POTENTIAL;
                    } else{
                       // log.debug( { title: 'elite status', details: 'no status' }); 
                        eliteStatus = SCRIPT_PARAMETERS.ELITE_STATUS.NO_STATUS;
                    }
                } else if(monthsActiveLast3Y > 12){
                    if(revenuePerActiveMonthLast3Y > 10000){
                        //log.debug( { title: 'elite status', details: 'Top - MVP of the season' });
                        eliteStatus = SCRIPT_PARAMETERS.ELITE_STATUS.TOP_MVP_OF_THE_SEASON;
                    } else if(revenuePerActiveMonthLast3Y > 5000){
                        //log.debug( { title: 'elite status', details: 'MVP potential' });
                        eliteStatus = SCRIPT_PARAMETERS.ELITE_STATUS.MVP_POTENTIAL;
                    } else{
                       // log.debug( { title: 'elite status', details: 'no status' }); 
                        eliteStatus = SCRIPT_PARAMETERS.ELITE_STATUS.NO_STATUS;
                    }
                } else if(monthsActiveLast3Y > 6) {
                    if(revenuePerActiveMonthLast3Y > 10000){
                       // log.debug( { title: 'elite status', details: 'Rockie of the Year Team' });
                        eliteStatus = SCRIPT_PARAMETERS.ELITE_STATUS.ROOKIE_OF_THE_YEAR_TEAM;
                    } else{
                       // log.debug( { title: 'elite status', details: 'no status' }); 
                        eliteStatus = SCRIPT_PARAMETERS.ELITE_STATUS.NO_STATUS;
                    }
                } else {
                    //log.debug( { title: 'elite status', details: 'no status' }); 
                    eliteStatus = SCRIPT_PARAMETERS.ELITE_STATUS.NO_STATUS;
                }
            } 
            
            //third subtab
            const totalTimeList = [];
            //calculating total productive and utilized time
            const totalTimeUtilized = _getTotalTime(customerid, 'utilized', '');
            totalTimeList.push(totalTimeUtilized);
            //log.debug('totalTimeUtilized', totalTimeUtilized);
    
            const totalTimeProductive = _getTotalTime(customerid, 'productive', '');
            totalTimeList.push(totalTimeProductive);
            //log.debug('totalTimeProductive', totalTimeProductive);
            
            const totalTime = totalTimeUtilized + totalTimeProductive;
            let totalTimeUtilizedPercentage;
            if(totalTime > 0){
                totalTimeUtilizedPercentage = totalTimeUtilized / totalTime;
                totalTimeUtilizedPercentage = _getPercentage(totalTimeUtilizedPercentage);
                //log.debug('totalTimeUtilizedPercentage', totalTimeUtilizedPercentage);
            } else {
                totalTimeUtilizedPercentage = 0;
            }
    
            //calculating support productive and utilized time
            const totalSupportTimeUtilized = _getTotalTime(customerid, 'utilized', 'support');
            totalTimeList.push(totalSupportTimeUtilized);
            //log.debug('totalSupportTimeUtilized', totalSupportTimeUtilized);  
            
            const totalSupportTimeProductive = _getTotalTime(customerid, 'productive', 'support');
            totalTimeList.push(totalSupportTimeProductive);
            //log.debug('totalSupportTimeProductive', totalSupportTimeProductive);  
            
            const totalSupportTime = totalSupportTimeUtilized + totalSupportTimeProductive;
            let totalSupportTimeUtilizedPercentage;
            if(totalSupportTime > 0){
                totalSupportTimeUtilizedPercentage = totalSupportTimeUtilized/ totalSupportTime;
                totalSupportTimeUtilizedPercentage = _getPercentage(totalSupportTimeUtilizedPercentage);
                //log.debug('totalSupportTimeUtilizedPercentage', totalSupportTimeUtilizedPercentage); 
            }else{
                totalSupportTimeUtilizedPercentage = 0;
            }

            
            //calculating dedicated resource productive and utilized time
            const totalDedicatedtTimeUtilized = _getTotalTime(customerid, 'utilized', 'dedicated');
            totalTimeList.push(totalDedicatedtTimeUtilized);
            //log.debug('totalDedicatedtTimeUtilized', totalDedicatedtTimeUtilized);
            
            const totalDedicatedtTimeProductive = _getTotalTime(customerid, 'productive', 'dedicated');
            totalTimeList.push(totalDedicatedtTimeProductive);
            //log.debug('totalDedicatedtTimeUtilized', totalDedicatedtTimeProductive); 

            const totalDedicatedTime = totalDedicatedtTimeUtilized + totalDedicatedtTimeProductive;
            let totalDedicatedTimeUtilizedPercentage;
            if(totalDedicatedTime > 0){
                totalDedicatedTimeUtilizedPercentage = totalDedicatedtTimeUtilized/ totalDedicatedTime;
                totalDedicatedTimeUtilizedPercentage = _getPercentage(totalDedicatedTimeUtilizedPercentage);
                //log.debug('totalDedicatedTimeUtilizedPercentage', totalDedicatedTimeUtilizedPercentage);  
            }else{
                totalDedicatedTimeUtilizedPercentage = 0;
            }
           
            
            //calculating fix bid productive and utilized time
            const totalFixTimeUtilized = _getTotalTime(customerid, 'utilized', 'fix');
            totalTimeList.push(totalFixTimeUtilized);
            //log.debug('totalFixTimeUtilized', totalFixTimeUtilized);
            
            const totalFixTimeProductive = _getTotalTime(customerid, 'productive', 'fix');
            totalTimeList.push(totalFixTimeProductive);
            //log.debug('totalFixTimeProductive', totalFixTimeProductive);    
            
            const totalFixTime = totalFixTimeUtilized + totalFixTimeProductive;
            let totalFixTimeUtilizedPercentage;
            if(totalFixTime > 0){
                totalFixTimeUtilizedPercentage = totalFixTimeUtilized/ totalFixTime;
                totalFixTimeUtilizedPercentage = _getPercentage(totalFixTimeUtilizedPercentage);
                //log.debug('totalFixTimeUtilizedPercentage', totalFixTimeUtilizedPercentage);
            }else{
                totalFixTimeUtilizedPercentage = 0;
            } 

            //calculating total productive and  utilized time | this year
            const totalTimeUtilizedThisYear = _getTotalTime(customerid, 'utilized', '', lastYears = 1);
            totalTimeList.push(totalTimeUtilizedThisYear);
            log.debug('totalTimeUtilizedThisYear', totalTimeUtilizedThisYear);

            const totalTimeProductiveThisYear = _getTotalTime(customerid, 'productive', '', lastYears = 1);
            totalTimeList.push(totalTimeProductiveThisYear);
            log.debug('totalTimeProductiveThisYear', totalTimeProductiveThisYear);


            //calculating total productive and  utilized time | last year
            const totalTimeUtilizedLastYear = _getTotalTime(customerid, 'utilized', '', lastYears = 2);
            totalTimeList.push(totalTimeUtilizedLastYear);
            log.debug('totalTimeUtilizedLastYear', totalTimeUtilizedLastYear);

            const totalTimeProductiveLastYear = _getTotalTime(customerid, 'productive', '', lastYears = 2);
            totalTimeList.push(totalTimeProductiveLastYear);
            log.debug('totalTimeProductiveLastYear', totalTimeProductiveLastYear);
            
            const quarterLabels = _getQuarterLabels();

            const monthLabels = _getMonthLabels();
            
            /**setting values */
            record.submitFields({
                type: record.Type.CUSTOMER,
                id: customerid,
                values: {
                    'custentity_ac_total_revenue': totalAmount,
                    'custentity_ac_elite_status': eliteStatus,
                    'custentity_ac_first_invoice_date': firstInvoiceDate == SCRIPT_PARAMETERS.DO_NOT_APPLY.VALUE ? SCRIPT_PARAMETERS.DO_NOT_APPLY.LABEL: firstInvoiceDate,
                    'custentity_ac_last_invoice_date': lastInvoiceDate == SCRIPT_PARAMETERS.DO_NOT_APPLY.VALUE ? SCRIPT_PARAMETERS.DO_NOT_APPLY.LABEL: lastInvoiceDate,
                    'custentity_ac_months_inactive' : monthsInactive == SCRIPT_PARAMETERS.DO_NOT_APPLY.VALUE ? SCRIPT_PARAMETERS.DO_NOT_APPLY.LABEL: monthsInactive,
                    'custentity_ac_months_active' : monthsActive == SCRIPT_PARAMETERS.DO_NOT_APPLY.VALUE ? SCRIPT_PARAMETERS.DO_NOT_APPLY.LABEL: monthsActive,
                    'custentity_ac_revenue_per_active_months' : revenuePerActiveMonth == SCRIPT_PARAMETERS.DO_NOT_APPLY.VALUE ? SCRIPT_PARAMETERS.DO_NOT_APPLY.LABEL: revenuePerActiveMonth.toFixed(2),
                    'custentity_ac_totaltime_utilized_percent' : totalTimeUtilizedPercentage, 
                    'custentity_ac_totaltime_utilized' : totalTimeUtilized, 
                    'custentity_ac_totaltime_productive' : totalTimeProductive, 

                    'custentity_ac_totaltime_utiliz_this_year' : totalTimeUtilizedThisYear, 
                    'custentity_ac_totaltime_produc_this_year' : totalTimeProductiveThisYear, 
                    'custentity_ac_totaltime_utiliz_last_year' : totalTimeUtilizedLastYear, 
                    'custentity_ac_totaltime_produc_last_year' : totalTimeProductiveLastYear, 

                    'custentity_ac_support_time_utilized_perc' : totalSupportTimeUtilizedPercentage, 
                    'custentity_ac_total_support_utilized' : totalSupportTimeUtilized, 
                    'custentity_ac_total_support_productive' : totalSupportTimeProductive, 
                    'custentity_ac_dr_utilized_percent' : totalDedicatedTimeUtilizedPercentage,
                    'custentity_ac_total_dr_utilized' : totalDedicatedtTimeUtilized,
                    'custentity_ac_total_dr_productive' : totalDedicatedtTimeProductive, 
                    'custentity_ac_fix_bid_time_utilized_perc' : totalFixTimeUtilizedPercentage,
                    'custentity_ac_total_fix_bid_utilized' : totalFixTimeUtilized,
                    'custentity_ac_total_fix_bid_productive' : totalFixTimeProductive,
                    'custentity_ac_last_year_q1' : averagePerQuarter[0],
                    'custentity_ac_last_year_q2' : averagePerQuarter[1],
                    'custentity_ac_last_year_q3' : averagePerQuarter[2],
                    'custentity_ac_last_year_q4' : averagePerQuarter[3],
                    'custentity_ac_current_year_q1' : averagePerQuarter[4],
                    'custentity_ac_current_year_q2' : averagePerQuarter[5],
                    'custentity_ac_current_year_q3' : averagePerQuarter[6],
                    'custentity_ac_current_year_q4' : averagePerQuarter[7],

                    'custentity_ac_last_year_jan' : amountPerMonth[0],
                    'custentity_ac_last_year_feb' : amountPerMonth[1],
                    'custentity_ac_last_year_mar' : amountPerMonth[2],
                    'custentity_ac_last_year_apr' : amountPerMonth[3],
                    'custentity_ac_last_year_may' : amountPerMonth[4],
                    'custentity_ac_last_year_jun' : amountPerMonth[5],
                    'custentity_ac_last_year_jul' : amountPerMonth[6],
                    'custentity_ac_last_year_aug' : amountPerMonth[7],
                    'custentity_ac_last_year_sep' : amountPerMonth[8],
                    'custentity_ac_last_year_oct' : amountPerMonth[9],
                    'custentity_ac_last_year_nov' : amountPerMonth[10],
                    'custentity_ac_last_year_dec' : amountPerMonth[11],


                    'custentity_ac_current_year_jan' : amountPerMonth[12],
                    'custentity_ac_current_year_feb' : amountPerMonth[13],
                    'custentity_ac_current_year_mar' : amountPerMonth[14],
                    'custentity_ac_current_year_apr' : amountPerMonth[15],
                    'custentity_ac_current_year_may' : amountPerMonth[16],
                    'custentity_ac_current_year_jun' : amountPerMonth[17],
                    'custentity_ac_current_year_jul' : amountPerMonth[18],
                    'custentity_ac_current_year_aug' : amountPerMonth[19],
                    'custentity_ac_current_year_sep' : amountPerMonth[20],
                    'custentity_ac_current_year_oct' : amountPerMonth[21],
                    'custentity_ac_current_year_nov' : amountPerMonth[22],
                    'custentity_ac_current_year_dec' : amountPerMonth[23],

                    'custentity_ac_quarter_labels' : JSON.stringify(quarterLabels),
                    'custentity_ac_average_per_quarter': JSON.stringify(averagePerQuarter),
                    'custentity_ac_month_labels' : JSON.stringify(monthLabels),
                    'custentity_ac_amount_per_month': JSON.stringify(amountPerMonth),

                    'custentity_ac_total_time_values' : JSON.stringify(totalTimeList),
                    'custentity_ac_1_invoice_date_last_3y' : firstInvoiceDateLast3Y,
                    'custentity_ac_last_invoice_date_last_3y' : lastInvoiceDateLast3Y,
                    'custentity_ac_total_revenue_last_3y' : totalAmountLast3Y,
                    'custentity_ac_months_active_last_3y' : monthsActiveLast3Y,
                    'custentity_ac_revenue_per_month_last_3y' : revenuePerActiveMonthLast3Y,
                    'custentity_ac_avg_rev_per_mth_this_year': averageRevenuePerMonthThisYear.toFixed(2),
                    'custentity_ac_avg_rev_per_mth_last_year': averageRevenuePerMonthLastYear.toFixed(2),
                    'custentity_ac_clv_progress': clvProgres.toFixed(2)
                }
            });
            
            //log.audit('customerid update: ', customerid);
            
        }catch(e){

            const errorObj = {
                name: e.name, 
                message: e.message, 
                stack: e.stack, 
                context: context.value
            };

           log.error({
               title: 'ERROR_ACCOUNT_ANALYSIS_MAP_STAGE',
               details: errorObj					
           });
        }
    }

    /**
     * Executes when the reduce entry point is triggered and applies to each group.
     *
     * @param {ReduceSummary} context - Data collection containing the groups to process through the reduce stage
     * @since 2015.1
     */
    function reduce(context) {

    }


    /**
     * Executes when the summarize entry point is triggered and applies to the result set.
     *
     * @param {Summary} summary - Holds statistics regarding the execution of a map/reduce script
     * @since 2015.1
     */
    function summarize(summary) {

    }


    function _getTotalInvoiceAmount(customerid, lastYears){

        filters = [
            ['type', search.Operator.ANYOF,'CustInvc', 'CustCred'], //invoices - credit memos | it is done automatically from NetSuite 
            'AND', 
            ['mainline', search.Operator.IS,'T'], 
            'AND', 
            ['customermain.internalid', search.Operator.ANYOF, customerid], 
            'AND', 
            ['status', search.Operator.ANYOF,'CustInvc:B','CustInvc:A', 'CustCred:B','CustCred:A'], // invoice:Open | invoice: paid in full | credit memo: fully applied | credit memo: open
         ];

        if(lastYears == 3) {
            filters.push('AND');
            filters.push(['trandate', search.Operator.ONORAFTER,'threefiscalyearsagotodate']);
        } else if(lastYears == 2){
            filters.push('AND');
            filters.push(['trandate', search.Operator.ONORAFTER,'startoflastfiscalyear']);
            filters.push('AND');
            filters.push(['trandate', search.Operator.ONORBEFORE,'lastfiscalyear']);
        } else if(lastYears == 1){
            filters.push('AND');
            filters.push(['trandate', search.Operator.ONORAFTER,'startofthisfiscalyear']);
        }       

        const invoiceSearchObj = search.create({
            type: search.Type.TRANSACTION,
            filters: filters,
            columns:
            [
               search.createColumn({
                  name: 'amount',
                  summary: search.Summary.SUM,
                  label: 'Amount'
               })
            ]
        });

        const searchResult = invoiceSearchObj.run().getRange({
            start: 0,
            end: 100
        });

        const totalAmount = searchResult[0].getValue({
            name: 'amount',
            summary: search.Summary.SUM
        }); 
        
        if(!_isNullOrEmpty(totalAmount)){
            return parseFloat(totalAmount); 
        } else {
            return parseFloat(0); 
        }
               
    }

    function _getTransactionDate(customerid, operator, lastYears){

        var filters = [
            ['type', search.Operator.ANYOF,'CustInvc'], 
            'AND', 
            ['mainline', search.Operator.IS,'T'], 
            'AND', 
            ['customermain.internalid', search.Operator.ANYOF, customerid], 
            'AND', 
            ['status', search.Operator.ANYOF,'CustInvc:A','CustInvc:B']
        ];


        if(lastYears == 3){
            filters.push('AND');
            filters.push(['trandate', search.Operator.ONORAFTER,'threefiscalyearsagotodate']);
        } else if(lastYears == 2) {
            filters.push('AND');
            filters.push(['trandate', search.Operator.ONORAFTER,'startoflastfiscalyear']);
            filters.push('AND');
            filters.push(['trandate', search.Operator.ONORBEFORE,'lastfiscalyear']);
        } else if(lastYears == 1) {
            filters.push('AND');
            filters.push(['trandate', search.Operator.ONORAFTER,'startofthisfiscalyear']);
        }

        const invoiceSearchObj = search.create({
            type: search.Type.INVOICE,
            filters: filters,
            columns:
            [
               search.createColumn({
                  name: 'trandate',
                  summary: operator == 'FIRST'? search.Summary.MIN: search.Summary.MAX,
                  label: 'Date'
               })
            ]
         });

         const searchResult = invoiceSearchObj.run().getRange({
            start: 0,
            end: 100
        });

        

        let transactiondate;

        if(!_isNullOrEmpty(searchResult)){

            transactiondate = searchResult[0].getValue({
                name: 'trandate',
                summary: operator == 'FIRST'? search.Summary.MIN: search.Summary.MAX,
            });

        } else{
            transactiondate = SCRIPT_PARAMETERS.DO_NOT_APPLY.VALUE;
        }

        return transactiondate;
    }    

    function _monthsDiff(d1 , d2) {
        let date1 = new Date(d1);
        let date2 = new Date(d2);
        let years = _yearsDiff(d1, d2);
        let months =(years * 12) + (date2.getMonth() - date1.getMonth());
        return parseInt(months);
    }    

    function _yearsDiff(d1, d2) {
        let date1 = new Date(d1);
        let date2 = new Date(d2);
        let yearsDiff =  date2.getFullYear() - date1.getFullYear();
        return yearsDiff;
    } 
    
    function _getTotalTime(customerid, typeTime, category, lastYears){

        let filters = [
            ['type', search.Operator.ANYOF,'A'], 
            'AND', 
            ['job.customer',search.Operator.ANYOF, customerid]
         ];

        if(typeTime == 'utilized'){
            filters.push('AND');
            filters.push(['utilized',search.Operator.IS,'T']);
        }

        if(typeTime == 'productive'){
            filters.push('AND');
            filters.push(['productive',search.Operator.IS,'T']);
        }
        
        if(category == 'support'){
            filters.push('AND');
            filters.push(['job.category',search.Operator.ANYOF, '6', '1']); //TSUITE project || TSUITE account > Legacy, it is not longer active among the filters         
        }

        if(category == 'dedicated'){
            filters.push('AND');
            filters.push(['job.category',search.Operator.ANYOF, '8']); //Dedicated Resource
        } 
        
        if(category == 'fix'){
            filters.push('AND');
            filters.push(['job.category',search.Operator.ANYOF, '2']); //Dedicated Resource            
        }

        if(lastYears == 1){
            filters.push('AND');
            filters.push(['date','onorafter','startofthisyear']);
        }

        if(lastYears == 2){
            filters.push('AND');
            filters.push(['date','onorafter','startoflastfiscalyear']);
            filters.push('AND');
            filters.push(['date','onorbefore','lastfiscalyear']);
        }

        var timebillSearchObj = search.create({
            type: 'timebill',
            filters: filters,
            columns:
            [
               search.createColumn({
                  name: 'durationdecimal',
                  summary: search.Summary.SUM,
                  label: 'Duration (Decimal)'
               })
            ]
         });
         var searchResultCount = timebillSearchObj.runPaged().count;


         const searchResult = timebillSearchObj.run().getRange({
            start: 0,
            end: 100
        });

        let totalTime = searchResult[0].getValue({
            name: 'durationdecimal',
            summary: search.Summary.SUM
        });

        if(!_isNullOrEmpty(totalTime)){
            return parseFloat(totalTime); 
        } else {
            return parseFloat(0); 
        }        
    }    

    function _getPercentage(total) {
        total = total * 100;
        total = total.toFixed(2);
        
        return total;
    }

    function _getAverageAmountPerQuarter(customerid){

        let today = new Date();
        const years = [];
        let currentYear = today.getFullYear();
        //adding years to get quarter totals
        years.push(currentYear-1);
        years.push(currentYear);

        let quarters = [1, 2, 3, 4];

        let columns = [];
        let averagePerQuarter = [];

        for(let y=0; y < years.length; y++){

            for(let q=0; q< quarters.length; q++){
                let columnName = `formulacurrency_${years[y]}_Q${quarters[q]}`;            
                let columnFormula = `(case when to_char({trandate},'Q/YYYY') = '${quarters[q]}/${years[y]}' then {amount} else 0 end)/3`;

                columns.push(search.createColumn({
                    name: columnName,
                    summary: search.Summary.SUM,
                    formula: columnFormula
                 })
                ); 
            }
        }
    
        const invoiceSearchObj = search.create({
            type: search.Type.TRANSACTION,
            filters:
            [
               ['customermain.internalid', search.Operator.ANYOF, customerid], 
               'AND', 
               ['type', search.Operator.ANYOF,'CustInvc', 'CustCred'], 
               'AND', 
               ['mainline', search.Operator.IS,'T']
            ],
            columns: columns
         });

         const searchResult = invoiceSearchObj.run().getRange({
            start: 0,
            end: 100
        });  

        /**getting results from scripting saved search */
        for(let index in searchResult){

            for(let y=0; y < years.length; y++){
                 for(let q=0; q< quarters.length; q++){
                     let columnName = `formulacurrency_${years[y]}_Q${quarters[q]}`;

                     const average = searchResult[index].getValue({
                        name: columnName,
                        summary: search.Summary.SUM
                    });

                    averagePerQuarter.push(average);
                
                 }
             }            
        } 
        
        return averagePerQuarter;

    }
    
    function _getAmountPerMonth(customerid){

        let today = new Date();
        const years = [];
        let currentYear = today.getFullYear();
        //adding years to get quarter totals
        years.push(currentYear-1);
        years.push(currentYear);

        let months = ['01', '02', '03', '04', '05', '06', '07', '08', '09', '10', '11', '12'];

        let columns = [];
        let amountPerMonth = [];

        for(let y=0; y < years.length; y++){

            for(let m=0; m< months.length; m++){
                let columnName = `formulacurrency_${years[y]}_M${months[m]}`;            
                let columnFormula = `(case when to_char({trandate},'MM/YYYY') = '${months[m]}/${years[y]}' then {amount} else 0 end)`;

                columns.push(search.createColumn({
                    name: columnName,
                    summary: search.Summary.SUM,
                    formula: columnFormula
                 })
                ); 
            }
        }

        const invoiceSearchObj = search.create({
            type: search.Type.TRANSACTION,
            filters:
            [
               ['customermain.internalid', search.Operator.ANYOF, customerid], 
               'AND', 
               ['type', search.Operator.ANYOF,'CustInvc', 'CustCred'], 
               'AND', 
               ['mainline', search.Operator.IS,'T']
            ],
            columns: columns
         });

         const searchResult = invoiceSearchObj.run().getRange({
            start: 0,
            end: 100
        });  

        /**getting results from scripting saved search */
        for(let index in searchResult){

            for(let y=0; y < years.length; y++){
                 for(let m=0; m< months.length; m++){
                     let columnName = `formulacurrency_${years[y]}_M${months[m]}`;

                     const amount = searchResult[index].getValue({
                        name: columnName,
                        summary: search.Summary.SUM
                    });

                    amountPerMonth.push(amount);
                
                 }
             }            
        } 
        
        return amountPerMonth;

    }
    
    function _getQuarterLabels(){

        const quarterLabels = [];

        let today = new Date();
        const years = [];
        let currentYear = today.getFullYear();
        //adding years to get quarter totals
        years.push(currentYear-1);
        years.push(currentYear);

        let quarters = [1, 2, 3, 4];

        for(let y=0; y < years.length; y++){

            for(let q=0; q< quarters.length; q++){
                let quarterLabel = `${years[y]}_Q${quarters[q]}`;

                quarterLabels.push(quarterLabel);
            }
        }
        
        return quarterLabels;
    }

    function _getMonthLabels(){

        const monthLabels = [];

        let today = new Date();
        const years = [];
        let currentYear = today.getFullYear();
        //adding years to get quarter totals
        years.push(currentYear-1);
        years.push(currentYear);

        let months = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];

        for(let y=0; y < years.length; y++){

            for(let m=0; m< months.length; m++){
                let monthLabel = `${years[y]}_${months[m]}`;

                monthLabels.push(monthLabel);
            }
        }
        
        return monthLabels;
    }
    
    /** UTILITIES */
	function _isNullOrEmpty(value) {
		if ((value === '') || (value === null) || (typeof value === 'undefined')) {
			return true;
		} else if (value instanceof String) {
			if (value.trim() === '') {
				return true;
			}
		} else if (Array.isArray(value)) {
			if (value.length === 0) {
				return true;
			}
		} else if (typeof value == 'object') {
			for (let key in value) {
				if (value.hasOwnProperty(key)) {
					return false;
				}
			}
			return true;
		} else {
			return false;
		}
    }    

    return {
        getInputData: getInputData,
        map: map,
        reduce: reduce,
        summarize: summarize
    };
    
});
